;;; anywhere-mode.el --- Anywhere mode

;; Copyright © 2018 Oleg Pykhalov <go.wigust@gmail.com>

;; This file is part of Emacs-Anywhere.

;; Emacs-Anywhere is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Emacs-Anywhere is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with Emacs-Anywhere.
;; If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides `anywhere-create', the command to open
;; `anywhere-buffer'.
;;
;; Inspired by <https://github.com/zachcurry/emacs-anywhere>.

;;; Code:

(require 'subr-x)

(defcustom anywhere-buffer "*Anywhere*"
  "Buffer name for anywhere-* commands."
  :type 'string
  :group 'anywhere-mode)

(defcustom anywhere-kill-buffer t
  "Kill buffer in `anywhere-exit' function."
  :type 'boolean
  :group 'anywhere-mode)

(defcustom anywhere-kill-frame t
  "Delete frame in `anywhere-exit' function."
  :type 'boolean
  :group 'anywhere-mode)

(defcustom anywhere-major-mode 'markdown-mode
  "Major mode in `anywhere-buffer' buffer."
  :type 'function
  :group 'anywhere-mode)

(defvar anywhere-command
  (mapconcat 'identity
             (list "@XCLIP_BIN@"
                   "-selection" "clipboard" "&>" "/dev/null")
             " "))

(defvar anywhere-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c . q") 'anywhere-exit)
    (define-key map (kbd "C-c . l") 'anywhere-erase-buffer)
    map)
  "Key map used by `anywhere-mode'.")

(defun anywhere-mode-p (buffer)
  "Return non-nil if BUFFER contains `anywhere-mode'."
  (buffer-local-value 'anywhere-mode (get-buffer buffer)))

(defun anywhere-buffer-p (buffer)
  "Return non-nil if BUFFER name has a `anywhere-buffer' prefix."
  (string-prefix-p anywhere-buffer (buffer-name buffer)))

(defmacro anywhere-with-buffer (buffer &rest body)
  "Return an error if BUFFER not an `anywhere-buffer' buffer
and `anywhere-mode' is not enabled for a BUFFER."
  `(if (and (anywhere-buffer-p ,buffer) (anywhere-mode-p ,buffer))
       (progn ,@body)
     (error "Currently not in `anywhere-buffer' buffer\
 or `anywhere-mode' is not enabled.")))

(defun anywhere-exit ()
  "Copy `anywhere-buffer' to GUI's clipboard.
If `anywhere-kill-buffer' is non-nil kill current buffer.
If `anywhere-kill-frame' is non-nil delete current frame."
  (interactive)
  (let ((buffer (current-buffer)))
    (anywhere-with-buffer buffer
      (with-current-buffer buffer
        (shell-command-on-region (point-min) (point-max)
                                 anywhere-command))
      (when anywhere-kill-buffer (kill-buffer buffer))
      (when anywhere-kill-frame (delete-frame)))))

;;;###autoload
(defun anywhere-erase-buffer ()
  "Erase current buffer."
  (interactive)
  (unless (string-blank-p
           (filter-buffer-substring (point-min) (point-max)))
    (erase-buffer)))

;;;###autoload
(define-minor-mode anywhere-mode
  "Auxiliary minor mode used when editing `anywhere-buffer'."
  nil " Anywhere" anywhere-mode-map
  (setq-local header-line-format
              (substitute-command-keys
               "Edit, then exit with `\\[anywhere-exit]'.")))

;;;###autoload
(defun anywhere-create ()
  "Create a `anywhere-buffer' buffer with `anywhere-mode' and
switch to it."
  (let ((buffer (get-buffer-create anywhere-buffer)))
    (switch-to-buffer buffer)
    (anywhere-erase-buffer)
    (funcall anywhere-major-mode)
    (unless (anywhere-mode-p (current-buffer))
      (anywhere-mode))))

(provide 'anywhere-mode)

;;; anywhere-mode.el ends here
